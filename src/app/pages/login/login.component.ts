import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../theme/services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(
    private _loginService:LoginService
  ) { }

  ngOnInit() {
  }

  milogin(){
    this._loginService.loginSpotify()
      .subscribe(result => {
        console.log("- result: ", result);
        
        this._loginService.setToken(result.access_token);
      });
  }
  miservicio(){
    this._loginService.dataSpotify()
      .subscribe(result => {
        console.log("- result dataSpotify: ", result);
      });
  }

}
