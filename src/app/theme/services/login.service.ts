import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpspotifyService } from '../http/httpspotify.service';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  public spotify = environment.spotikey;
  token:string= '';

  constructor(
    private router: Router,
    private httpspoti: HttpspotifyService
  ) {
    this.token = this.getToken();
  }

  loginSpotify(): Observable<any> {
    // https://accounts.spotify.com/api/token
    // https://spotify-get-token.herokuapp.com/spotify/{ClientID}/{ClientSecret}
    let ruta = `${this.spotify.spotiAccountUrl}/spotify/${this.spotify.client_id}/${this.spotify.client_secret}`;
    return this.httpspoti.get(ruta);
  }

  dataSpotify(): Observable<any>{
    // https://api.spotify.com/v1/tracks/2TpxZ7JUBn3uw46aR7qd6V
    let ruta = `${this.spotify.spotiUrl}/v1/tracks/2TpxZ7JUBn3uw46aR7qd6V`;

    return this.httpspoti.post(ruta, this.token);
  }

  logoutSpotify(){}

  getToken(): string {
    return localStorage.getItem('token');
  }

  setToken(token: string) {
    localStorage.setItem('token', token);
    this.token = this.getToken();
  }

  removeToken() {
    localStorage.removeItem('token');
    this.token = this.getToken();
  }

  loggedIn(): boolean {
    if (this.getToken()) {
      return true;
    } else {
      return false;
    }
  }
}
