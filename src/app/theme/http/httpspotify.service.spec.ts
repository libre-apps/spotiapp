import { TestBed } from '@angular/core/testing';

import { HttpspotifyService } from './httpspotify.service';

describe('HttpspotifyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HttpspotifyService = TestBed.get(HttpspotifyService);
    expect(service).toBeTruthy();
  });
});
