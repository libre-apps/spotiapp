import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpspotifyService {

  token:any;
  constructor(
    private http: HttpClient
  ) {
  }

  get(url: string): Observable<any>{
    return this.http.get(url);
  }
  post(url: string, data: any): Observable<any> {
    return this.http.post(url, data);
  }
  postAccountSpotify(url: string, data: any): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/x-www-form-urlencoded',
        "cache-control": "no-cache"
      })
    };
    return this.http.post(url, data, httpOptions);
  }
  delete(url: string): Observable<any> {
    return this.http.delete(url);
  }
  put(url: string, data: any): Observable<any> {
    return this.http.put(url, data);
  }
  patch(url: string, data: any): Observable<any> {
    return this.http.patch(url, data);
  }

  // postHeader(url: string, data: any, token: string): Observable<any> {
  //   const httpOptions = {
  //     headers: new HttpHeaders({
  //       'Content-Type':  'application/json',
  //       'Authorization': `Bearer ${token}`
  //     })
  //   };
  //   return this.http.post(url, data, httpOptions);
  // }
  








  /********************************************************************************* */
  getHeader(url: string, token): Observable<any>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': `Bearer ${token}`
      })
    };
    return this.http.get(url, httpOptions);
  }
  // post(url: string, data: any): Observable<any> {
  //   this.mitoken();
  //   if(this.token){
  //     const httpOptions = {
  //       headers: new HttpHeaders({
  //         'Content-Type':  'application/json',
  //         'Authorization': `Bearer ${this.token}`
  //       })
  //     };
  //     return this.http.post(url, data, httpOptions);
  //   }else{
  //     return this.http.post(url, data);
  //   }
  // }
  // delete(url: string): Observable<any> {
  //   this.mitoken();
  //   if(this.token){
  //     const httpOptions = {
  //       headers: new HttpHeaders({
  //         'Content-Type':  'application/json',
  //         'Authorization': `Bearer ${this.token}`
  //       })
  //     };
  //     return this.http.delete(url, httpOptions);
  //   }else{
  //     return this.http.delete(url);
  //   }
  // }
  // put(url: string, data: any): Observable<any> {
  //   this.mitoken();
  //   if(this.token){
  //     const httpOptions = {
  //       headers: new HttpHeaders({
  //         'Content-Type':  'application/json',
  //         'Authorization': `Bearer ${this.token}`
  //       })
  //     };
  //     return this.http.put(url, data, httpOptions);
  //   }else{
  //     return this.http.put(url, data);
  //   }
  // }
  // patch(url: string, data: any): Observable<any> {
  //   this.mitoken();
  //   if(this.token){
  //     const httpOptions = {
  //       headers: new HttpHeaders({
  //         'Content-Type':  'application/json',
  //         'Authorization': `Bearer ${this.token}`
  //       })
  //     };
  //     return this.http.patch(url, data, httpOptions);
  //   }else{
  //     return this.http.patch(url, data);
  //   }
  // }

  // mitoken(){
  //   this.token = localStorage.getItem('token');
  // }
}
