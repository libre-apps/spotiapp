import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { FormsModule, ReactiveFormsModule } from '@angular/forms';

/****** Servicios ******/
// Config
import { HttpspotifyService } from './theme/http/httpspotify.service';
import { LoginService } from './theme/services/login.service';
// Guard
import { LoginGuard } from './theme/guard/login.guard';
import { AuthGuard } from './theme/guard/auth.guard';

// Normales

/****** Enviroment ******/
import { environment } from '../environments/environment';

/****** Routes ******/
import { AppRoutingModule } from './app-routing.module';

/****** Componentes ******/
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { HeaderComponent } from './reusable/header/header.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    LoginGuard,
    AuthGuard,
    HttpspotifyService,
    LoginService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
